package com.ksquaregroup.project;

public class Item {

    private int priority;

    public int getPriority() {
        return priority;
    }

    public Item setPriority(int priority) {
        this.priority = priority;
        return this;
    }
}
