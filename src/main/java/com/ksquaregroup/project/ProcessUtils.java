package com.ksquaregroup.project;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProcessUtils {

    public static long countProcesses(List<Item> list) {

        List<Item> items = list.stream()
                .map(item -> new Item().setPriority(item.getPriority()))
                .collect(Collectors.toList());

        int count = 0;

        while(true) {
            int maxPriority = -1;
            int maxPriorityIndex = -1;
            int subMaxPriority = -1;

            for (int i = 0; i < items.size(); i++) {
                int priority = items.get(i).getPriority();
                if (maxPriority < priority) {
                    subMaxPriority = maxPriority;
                    maxPriority = priority;
                    maxPriorityIndex = i;
                } else if (subMaxPriority < priority && priority < maxPriority) {
                    subMaxPriority = priority;
                }
            }

            if (subMaxPriority == -1) {
                break;
            } else {
                count ++;
                items.get(maxPriorityIndex).setPriority(subMaxPriority);
            }
        }

        return count;
    }

    public static long countProcessesPro(List<Item> items) {

        Map<Integer, Integer> occurrences = new HashMap<>();
        items.stream()
                .map(Item::getPriority)
                .forEach(i -> {
                    if(occurrences.containsKey(i)){
                        occurrences.put(i, occurrences.get(i)+1);
                    } else {
                        occurrences.put(i, 1);
                    }
                });

        int count = 0;

        int index = 0;
        for (int frequency : occurrences.values()){
            count += index*frequency;
            index++;
        }

        return count;
    }
}

