package com.ksquaregroup.project;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MyBenchmark {

    final static int SIZE = 1000;
    final static int MAX_PRIORITY = 10;
    final static int SAMPLES = 10;

    @State(Scope.Benchmark)
    public static class Data {
        static Random random = new Random();
        static List<List<Item>> samples = new ArrayList<>(SAMPLES);

        static {
            List<Item> items = new ArrayList<>(SIZE);
            for (int i=0; i < SAMPLES; i++){
                for (int j = 0; j < SIZE; j++) {
                    items.add(new Item().setPriority(random.nextInt(MAX_PRIORITY)));
                }
                samples.add(items);
            }
        }
    }

    @Benchmark
    @Fork(value = 5, warmups = 2)
    @Warmup(iterations = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testCountProcesses(Data data, Blackhole blackhole) {
        for (List<Item> items : data.samples){
            blackhole.consume(ProcessUtils.countProcesses(items));
        }
    }

    @Benchmark
    @Fork(value = 5, warmups = 2)
    @Warmup(iterations = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testCountProcessesPro(Data data, Blackhole blackhole) {
        for (List<Item> items : data.samples){
            blackhole.consume(ProcessUtils.countProcessesPro(items));
        }
    }
}
